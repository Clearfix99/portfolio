 Rails.application.routes.draw do
  
  resources :topics, only: [:index, :show]

  devise_for :users, path: '', path_names: {sign_in: 'login', sign_out: 'logout', sign_up: 'register'}
# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

#set root to home page
root to: "pages#home"

#excludes show so that a custom route can be added for it
resources :dev_portfolios, except: [:show, :index] do
    put :sort, on: :collection
end
  get "portfolios", to: 'dev_portfolios#index'
  get "portfolios/new", to: 'dev_portfolios#new'
  #use as to generate a custom prefix as this route will not have a prefix by itself
  get 'portfolios/:id', to: 'dev_portfolios#show', as: "portfolio_show"
  get 'react_items', to: 'dev_portfolios#react'
resources :blogs do
	member do
		get :toggle_status 
	end
end

#this replaces get 'pages/about' which assumes that pages is the controller and contact is the action
  get 'about', to: 'pages#about'
#maps the route to controller on left of hash and action on right
  get 'contact', to: 'pages#contact'
# because it's mapped you can pass w/e you want in the url e.g. get 'asdfasd'


  # get 'tech_news', to: 'pages#tech_news'
 


end
