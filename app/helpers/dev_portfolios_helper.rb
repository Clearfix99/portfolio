module DevPortfoliosHelper

	def image_generator(height:, width:)
		"https://via.placeholder.com/#{height}x#{width} " 
	end
	
end

# self.main_image ||=Placeholder.image_generator(height: '600', width: '400')
# 	self.thumbnail ||=Placeholder.image_generator(height: '350', width: '200')
def placeholder_img img, type
	if img.model.main_image? || img.model.thumbnail?		
		img
	elsif type=='thumb'
		image_generator(height:'356', width:'250')
	elsif type =='main'
		image_generator(height:'600', width: '400')
	end 
		
	
end