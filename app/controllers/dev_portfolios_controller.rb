class DevPortfoliosController < ApplicationController
  layout "portfolio"
  access all: [:show, :index, :react], user: {except: [:destroy, :new, :create, :update, :edit, :sort]}, site_admin: :all

  def index
    # accessed from the react method in dev_portfolio.rb
  @portfolio=DevPortfolio.by_position
  end

  def sort
    params[:order].each do |key, value|
      DevPortfolio.find(value[:id]).update(position: value[:position])
    end
    render body: nil
  end

  def react
    @react_portfolio=DevPortfolio.react
  end
#making @portfolio available to view
  def new
    @portfolio=DevPortfolio.new
  end

#need to make it available and connect to db
  def create
    #params that are allowed
    @portfolio=DevPortfolio.new(dev_portfolio_params)
    
    respond_to do|format|
      if @portfolio.save
        format.html {redirect_to portfolios_path, notice: "Your portfolio item is now live"}
      else
        format.html{render :new}
      end
    end
  end

  def edit
    #pass in the id from article to direct rails to what you want edited
    @portfolio=DevPortfolio.find(params[:id])
  end

  def update
    @portfolio=DevPortfolio.find(params[:id])

    respond_to do |f|
      if @portfolio.update(dev_portfolio_params)
        f.html { redirect_to portfolios_path, notice: "Your portfolio item has been updated"}
      else
        f.html {render :edit}
      end
    end
  end
def show
  @portfolio=DevPortfolio.find(params[:id])
end

def destroy
  #perform look up
  @portfolio=DevPortfolio.find(params[:id])
  # destroy the record
  @portfolio.destroy
  #redirect
  respond_to do |f|
    f.html {redirect_to portfolios_url, notice: "This item has been deleted"}
    end
  end

private
#refactor strongg params for ease of access
  def dev_portfolio_params
    params.require(:dev_portfolio).permit(:title, 
                                          :subtitle,
                                          :body,
                                          :main_image,
                                          :thumbnail,
                                          technologies_attributes: [:id, :name, :_destroy]
                                          )
  end


end
