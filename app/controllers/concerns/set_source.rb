module SetSource
	extend ActiveSupport::Concern

	included do 
		before_action :set_source
	end
	#sessions are not secure do not put any spi into them
	def set_source
		session[:source] = params[:q] if params[:q]
		
	end 
end


