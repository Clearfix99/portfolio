module CurrentUserConcern
	extend ActiveSupport::Concern
	#this is a null object pattern
	def current_user
		#super tells the method to copy what already exists (as this is a method provied by devise)
		super || guest_user
	end

	def guest_user
		guest= GuestUser.new
		guest.name= "Guest User"
		guest.first_name= "Guest"
		guest.last_name= "User"
		guest.email= "test@email.com"
		#so that the full guest object is returned you ask for guest again at the end
		guest
	end
	
end


