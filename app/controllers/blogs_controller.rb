class BlogsController < ApplicationController
  before_action :set_blog, only: [:show, :edit, :update, :destroy, :toggle_status]
  before_action :set_sidebar_topics, except: [:create, :update, :destroy, :toggle_status]
  layout "blog"
  #access all= everyone can look at index and show regardless of their status
  #User except= general users cannot access the destroy method, commentors can't delete or create blogs
  #site_admin= the administrator has access to everything
  access all: [:show, :index], user: {except: [:destroy, :new, :create, :update, :edit, :toggle_status]}, site_admin: :all
  # GET /blogs
  # GET /blogs.json
  def index
    if logged_in?(:site_admin)
      @blogs = Blog.recent.page(params[:page]).per(5)
    else
      @blogs = Blog.published.recent.page(params[:page]).per(5)
    end
    @page_title="My Portfolio | Blog"
    @tweets=SocialTool.twitter_search

  end

  # GET /blogs/1
  # GET /blogs/1.json
  def show
       if logged_in?(:site_admin) || @blog.published?
      @blog = Blog.friendly.find(params[:id])
    else
      redirect_to blogs_path, notice: "You're not meant to see that"
    end
    @page_title=@blog.title
    @tweets=SocialTool.twitter_search
  end

  # GET /blogs/new
  def new
    @blog = Blog.new
    @tweets=SocialTool.twitter_search
  end

  # GET /blogs/1/edit
  def edit
    @tweets=SocialTool.twitter_search
  end

  # POST /blogs
  # POST /blogs.json
  def create
    @blog = Blog.new(blog_params)
    @tweets=SocialTool.twitter_search

    respond_to do |format|
      if @blog.save
        format.html { redirect_to @blog, notice: 'Blog was successfully created.' }
        format.json { render :show, status: :created, location: @blog }
      else
        format.html { render :new }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /blogs/1
  # PATCH/PUT /blogs/1.json
  def update
    @tweets=SocialTool.twitter_search
    respond_to do |format|
      if @blog.update(blog_params)
        format.html { redirect_to @blog, notice: 'Blog was successfully updated.' }
        format.json { render :show, status: :ok, location: @blog }
      else
        format.html { render :edit }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /blogs/1
  # DELETE /blogs/1.json
  def destroy
    @tweets=SocialTool.twitter_search
    @blog.destroy
    respond_to do |format|
      format.html { redirect_to blogs_url, notice: 'Blog was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
def toggle_status
    if @blog.draft?
      @blog.published!
     elsif @blog.published?
      @blog.draft!
    end
  redirect_to blogs_url, notice: "Post status updated"
end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_blog
      # overrides the default behavior
      @blog = Blog.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def blog_params
      params.require(:blog).permit(:title, :body, :topic_id, :status)
    end

  def set_sidebar_topics
        @side_bar_topics= Topic.with_blogs

  end

end
