class DevPortfolio < ApplicationRecord
	has_many :technologies
	accepts_nested_attributes_for :technologies, 
								   allow_destroy: true,
									reject_if: lambda {|attr| attr['name'].blank?}


	# include Placeholder
	validates_presence_of :title, :body

	mount_uploader :thumbnail, DevPortfolioUploader
	mount_uploader :main_image, DevPortfolioUploader



# using these methods is considered best practice as it forces you to keep all the logic for the model in the model file
	def self.react
		where(subtitle: 'React')
		
	end

	def self.by_position
		order("position ASC")
	end



# creating a custom scope using a lambda
	scope :ruby_on_rails, ->{where(subtitle: 'Ruby on Rails')}

#after a portfolio item is started a default image and thumbnail is added
	# after_initialize :set_defaults
	# def set_defaults
	# 	self.main_image ||=Placeholder.image_generator(height: '600', width: '400')
	# 	self.thumbnail ||=Placeholder.image_generator(height: '350', width: '200')
	# end



end
