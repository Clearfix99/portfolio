class Blog < ApplicationRecord
	enum status: { draft: 0, published: 1 }
	extend FriendlyId
	# This takes the title of the blogpost and creates a slug for the url
	friendly_id :title, use: :slugged

# requires any post to have a title and body before it will create
	validates_presence_of :title, :body, :topic_id
	belongs_to :topic, optional: true


	def self.recent
		order("created_at DESC")
	end
end
