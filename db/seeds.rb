# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

3.times do |topic|
  Topic.create!(
    title: "Topic #{topic}"
    )
end

10.times do |blog|
  Blog.create!(
  title: "My #{blog} Post",
  body:"Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nulla vitae elit libero, a pharetra augue. Nulla vitae elit libero, a pharetra augue. Maecenas faucibus mollis interdum. Maecenas sed diam eget risus varius blandit sit amet non magna.",
  topic_id: Topic.last.id
  )
end

5.times do |skill|
  Skill.create!(
  title: "Rails #{skill}",
  percent_utilized: 15
  )
end

8.times do |port|
  DevPortfolio.create!(
  title: "Portfolio Title: #{port}",
  subtitle: "Portfolio Subtitle",
  body: "Donec ullamcorper nulla non metus auctor fringilla. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum.",
  main_image:"https://via.placeholder.com/600x400",
  thumbnail: "https://via.placeholder.com/350x200"
  )
end
1.times do |port|
  DevPortfolio.create!(
  title: "Portfolio Title: #{port}",
  subtitle: "React",
  body: "Donec ullamcorper nulla non metus auctor fringilla. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum.",
  main_image:"https://via.placeholder.com/600x400",
  thumbnail: "https://via.placeholder.com/356x250"
  )
end

3.times do |tech|
  DevPortfolio.last.technologies.create!(
      name: "Technology #{tech}"
    )
end

puts "3 techs created"