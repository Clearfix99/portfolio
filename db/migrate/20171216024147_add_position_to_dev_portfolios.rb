class AddPositionToDevPortfolios < ActiveRecord::Migration[5.1]
  def change
    add_column :dev_portfolios, :position, :integer
  end
end
