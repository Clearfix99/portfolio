class CreateDevPortfolios < ActiveRecord::Migration[5.1]
  def change
    create_table :dev_portfolios do |t|
      t.string :title
      t.string :subtitle
      t.text :body
      t.text :main_image
      t.text :thumbnail

      t.timestamps
    end
  end
end
